<?php

?>
<!DOCTYPE HTML>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <title></title>
</head>
<body>
<h1>Create an Item</h1>
<form action="store.php" method="post">
    <fieldset>
        <legend>
            Add birthday
        </legend>
        <div>
            <label for="personName">Enter The Name</label>
            <input
                autofocus="autofocus"
                placeholder="enter your name"
                type="text" name="personName" id="personName" required="required" tabindex="3"/>
        </div>
        <div>
            <label for="birthday">Enter The Birthdate</label>
            <input
                autofocus="autofocus"
                placeholder="YYYY-mm-dd"
                type="text" name="birthday" id="birthday" required="required" tabindex="3"/>
        </div>
        <button type="submit">save</button>
        <button type="submit">save & Add Again</button>
        <input type="reset" value="reset"/>
    </fieldset>
</form>
</body>
</html>