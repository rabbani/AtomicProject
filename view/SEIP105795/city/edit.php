<?php session_start();
include_once($_SERVER["DOCUMENT_ROOT"].DIRECTORY_SEPARATOR."vendor".DIRECTORY_SEPARATOR."autoload.php");
use App\bitm\SEIP105795\City;
use \App\bitm\SEIP105795\Utility\Utility;

$obj=new City();
$theCity=$obj-> edit($_GET['id']);

?>
<!DOCTYPE HTML>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <title></title>
</head>
<body>
<h1>Edit an Item</h1>
<form action="update.php" method="post">
    <fieldset>
        <legend>
            Edit City
        </legend>
        <input type="hidden" name="id" value="<?php echo $theCity->id;?>"/>
        <div>
            <label for="cityName"> City Name</label>
            <input
                type="text" name="cityName" id="cityName" required="required" tabindex="3" value="<?php echo $theCity->cityName;?>"/>
        </div>
        <button type="submit">save</button>
        <button type="submit">save & Add Again</button>
        <input type="reset" value="reset"/>
    </fieldset>
</form>
<a href="index.php">Back to the list</a>
</body>
</html>