<?php
session_start();
include_once($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."vendor".DIRECTORY_SEPARATOR."autoload.php");
use App\bitm\SEIP105795\Book;
use App\bitm\SEIP105795\Utility\Utility;
$obj=new Book();
$b=$obj->view($_GET['id']);

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
<h1><?php echo $b->bookName;?></h1>
<dl>
    <dt>ID</dt>
    <dd><?php echo $b->id;?></dd>
    <dt>Author</dt>
    <dd><?php echo $b->author;?></dd>
</dl>
<nav>
    <li><a href="index.php">Go to list</a></li>
</nav>
</body>
</html>
