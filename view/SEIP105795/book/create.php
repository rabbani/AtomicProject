<?php

?>
<!DOCTYPE HTML>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <title></title>
</head>
<body>
    <h1>Create an Item</h1>
    <form action="store.php" method="post">        
        <fieldset>
            <legend>
                Add Book Title
            </legend>
            <div>
                <label for="bookName">Enter The Book Title</label>
                <input 
                    autofocus="autofocus"
                    placeholder="enter the book name"
                    type="text" name="bookName" id="bookName" required="required" tabindex="3"/>
            </div>
            <div>
                <label for="authorName">Enter Author Name</label>
                <input
                    autofocus="autofocus"
                    placeholder="enter author name"
                    type="text" name="author" id="authorName" required="required" tabindex="3"/>
            </div>
            <button type="submit">save</button>
            <button type="submit">save & Add Again</button>
            <input type="reset" value="reset"/>
        </fieldset>
    </form>
</body>
</html>