<?php

include_once($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."vendor/autoload.php");
use App\bitm\SEIP105795\Profile;
use App\bitm\SEIP105795\Message;
use App\bitm\SEIP105795\Utility;
$obj=new Profile();
$thePerson=$obj->edit($_GET['id']);
?>
<!DOCTYPE HTML>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <title></title>
</head>
<body>
<h1>Edit an Item</h1>
<form action="update.php" method="post">
    <fieldset>
        <legend>
            Edit Profile
        </legend>
        <input type="hidden" name="id" value="<?php echo $thePerson->id;?>"/>
        <div>
            <label for="NAME"> Person Name</label>
            <input
                autofocus="autofocus"
                placeholder="enter the person name"
                type="text" name="personName"id="name" required="required" tabindex="3" value="<?php echo $thePerson->name;?>"/>
        </div><div>
            <label for="roll"> Roll</label>
            <input
                autofocus="autofocus"
                placeholder="enter the person name"
                type="text" name="roll"id="roll" required="required" tabindex="3" value="<?php echo $thePerson->roll;?>"/>
        </div><div>
            <label for="batch">Batch Name</label>
            <input
                autofocus="autofocus"
                type="text" name="batch"id="batch" required="required" tabindex="3" value="<?php echo $thePerson->batch;?>"/>
        </div><div>
            <label for="phone">Phone Number</label>
            <input
                type="text" name="phone"id="phone" required="required" tabindex="3" value="<?php echo $thePerson->phone;?>"/>
        </div>
        <button type="submit">save</button>
        <button type="submit">save & Add Again</button>
        <input type="reset" value="reset"/>
    </fieldset>
</form>
<a href="index.php">Back to the list</a>
</body>
</html>